package com.example.habiteureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class HabitEurekaApplication {

	public static void main(String[] args) {
		SpringApplication.run(HabitEurekaApplication.class, args);
	}

}
